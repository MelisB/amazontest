package stepDefinition;

import java.awt.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;



public class AmazonTest {

	WebDriver driver;
	public WebDriverWait wait;
	String searchText = "electric violin";
	
	@Given("^Users opens Amazon.com$")
	public void openPage() throws Throwable {
		String exePath = "C:\\Users\\melis\\Desktop\\melis\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);
		driver = new ChromeDriver();
		wait = new WebDriverWait(driver, 600);
		driver.manage().window().maximize();  
		driver.get("https://www.amazon.com/");
		String link = "https://www.amazon.com/";
		String url = driver.getCurrentUrl();
		Assert.assertEquals(url,link);
	}
	
	@And("^User searches an item.$")
	public void searchItem() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("twotabsearchtextbox"))));
		if(driver.findElement(By.id("twotabsearchtextbox")).isDisplayed() == true) 
		{
			driver.findElement(By.id("twotabsearchtextbox")).sendKeys("electric violin");
			Thread.sleep(2000);
			driver.findElement(By.xpath("//div[@id='nav-search']/form/div[2]/div/input")).click();
			
	    }
	}
	
	@When("^User picks first item and go to detail page$")
	public void selectItem1() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("bcKwText"))));
		String textResult = driver.findElement(By.id("bcKwText")).getText();
		String result1 = driver.findElement(By.xpath("//div[3]/div/a/h2")).getText();
		driver.findElement(By.xpath("//div[3]/div/a/h2")).click();
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("breadcrumb-back-link"))));
		String detail1 = driver.findElement(By.id("productTitle")).getText();
		if(driver.findElement(By.id("productTitle")).isEnabled()== true) 
		{
			Assert.assertEquals(result1, detail1);
			driver.findElement(By.id("breadcrumb-back-link")).click();
			
		}
		
	}
	
	@And("^User picks second item an fo to detail page$")
	public void selectItem2() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("bcKwText"))));
		String textResult = driver.findElement(By.id("bcKwText")).getText();
		//Assert.assertEquals(textResult, searchText);
		String result2 = driver.findElement(By.xpath("//li[3]/div/div[4]/div/a/h2")).getText();
		driver.findElement(By.xpath("//li[3]/div/div[4]/div/a/h2")).click();
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("breadcrumb-back-link"))));
		String detail2 = driver.findElement(By.id("productTitle")).getText();
		if(driver.findElement(By.id("productTitle")).isDisplayed()== true) 
		{
			Assert.assertEquals(result2, detail2);
			
		}
	}
	
	@Then("^User picks compares item with the similars$")
	public void checkSimilar() throws Throwable {
		wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector("a.a-link-normal.HLCXComparisonJumplinkLink > span"))));
		driver.findElement(By.cssSelector("a.a-link-normal.HLCXComparisonJumplinkLink > span")).click();
		String price1 = driver.findElement(By.id("priceblock_ourprice")).getText();
		String price2 = driver.findElement(By.xpath("//tr[5]/td[2]/span/span[2]")).getText();
		String x = price1.replace("$", "");
		String y = price2.replace("$", "").replace("\n", ".");
		double number1 = Double.parseDouble(x);
		double number2 = Double.parseDouble(y);
		if (number1 > number2) {
			driver.findElement(By.id("comparison_add_to_cart_button-announce")).click();
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("hlb-ptc-btn-native"))));
			
			
		}else {
			driver.findElement(By.id("comparison_add_to_cart_button0-announce")).click();
			wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("hlb-ptc-btn-native"))));
		}
	
	}
		
	
	
	
	
	
	
		
		
		
}
	
	
	
	

